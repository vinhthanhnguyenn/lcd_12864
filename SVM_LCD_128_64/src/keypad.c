/*
 * keypad.c
 *
 *  Created on: 10 Sep 2022
 *      Author: Trung
 */

#include "keypad.h"

volatile uint32_t time_keypad = 0;
volatile uint16_t home_display_count = 0;
volatile uint16_t swap_home_display = 0;
volatile uint16_t dangthuchiengiaodich_count = 0;
volatile uint32_t timeout_return_home = 0;
volatile uint32_t read_DS3231_timeout = 0;


extern user_press_t purchase;
volatile uint8_t home_animation = NO_CHANGE;
volatile bool read_DS3231=false;


uint8_t keys[ROWS][COLS] =
{
{ '1', '2', '3' },
  { '4', '5', '6' },
  { '7', '8', '9' },
  { '*', '0', '#' } };

uint32_t COL[3] =
{ COL1, COL2, COL3 };
uint32_t ROW[4] =
{ ROW1, ROW2, ROW3, ROW4 };

void gpio_init(void)
{
    R_IOPORT_PinCfg (&g_ioport_ctrl, COL1, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, COL2, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, COL3, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, ROW1, IOPORT_CFG_PORT_DIRECTION_INPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, ROW2, IOPORT_CFG_PORT_DIRECTION_INPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, ROW3, IOPORT_CFG_PORT_DIRECTION_INPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, ROW4, IOPORT_CFG_PORT_DIRECTION_INPUT);

    R_IOPORT_PinWrite (&g_ioport_ctrl, COL[0], BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, COL[1], BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, COL[2], BSP_IO_LEVEL_LOW);
}
/**
 * Get value of button number from user press
 * @return value of button number
 */

uint8_t get_char_press(void)
{
    static volatile uint8_t col = 0, row = 0;
    static volatile uint8_t pin_status_change = 0;
    uint8_t character = 0;
    bsp_io_level_t pin_value = 0;

    R_IOPORT_PinWrite (&g_ioport_ctrl, COL[col], BSP_IO_LEVEL_HIGH);
    R_IOPORT_PinRead (&g_ioport_ctrl, ROW[row], &pin_value);
    if (pin_value == 1)
    {
        if (time_keypad == 0)
        {
            time_keypad = 1;
        }
        if ((time_keypad >= 15) && (pin_status_change == 0))
        {
            pin_status_change = 1;
        }
//        if (time_keypad >= 1500)
//        {
//            character = keys[row][col];
//            time_keypad = 0;
//            pin_status_change = 0;
//            R_IOPORT_PinWrite (&g_ioport_ctrl, COL[col], BSP_IO_LEVEL_LOW);
//            row++;
//        }
    }
    else
    {
        if (pin_status_change)
        {
            character = keys[row][col];
            pin_status_change = 0;
            R_IOPORT_PinWrite (&g_ioport_ctrl, COL[col], BSP_IO_LEVEL_LOW);
        }
        time_keypad = 0;
        row++;
    }
    if (row == ROWS)
    {
        row = 0;
        R_IOPORT_PinWrite (&g_ioport_ctrl, COL[col], BSP_IO_LEVEL_LOW);
        col++;
        if (col == COLS)
        {
            col = 0;
        }
    }
    return character;
}

/**
 * Keypad init function
 */
void keypad_init(void)
{
    R_IOPORT_Open (&g_ioport_ctrl, &g_bsp_pin_cfg);
    gpio_init ();
    R_GPT_Open (&g_timer0_ctrl, &g_timer0_cfg);
    R_GPT_Start (&g_timer0_ctrl);
}

void TIM0_Callback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_CYCLE_END)
    {
        if (time_keypad)
        {
            time_keypad++;
        }

//***************************************************** HOME SCREEN*****************************/
        if (purchase.state_old == screen_HOME_SCREEN)
        {
            timeout_return_home=0;
            swap_home_display++;
            read_DS3231_timeout++;

            if (home_animation != CHANGE2)
            {
                home_display_count++;
                if ((home_display_count == 500))
                {
                    home_animation = CHANGE1;
                    purchase.animation_change = true;

                }

                else if (home_display_count > 1000) //>1s
                {
                    home_animation = NO_CHANGE;
                    home_display_count = 0;
                    purchase.animation_change = true;

                }

                else
                {
                    purchase.animation_change = false;

                }
            }

            if (swap_home_display == 5000) //>5s
            {
                home_animation = CHANGE2;
                home_display_count = 0;
                purchase.animation_change = true;

            }

            else if (swap_home_display > 9000)
            {
                swap_home_display = 0;
                purchase.animation_change = true;
                home_animation = CHANGE3;

            }

        }

        else // State is not in screen_HOME_SCREEN
        {
            if(purchase.state==screen_SCAN_KEYPAD)
            {
                timeout_return_home++;
            }

            else
            {
                timeout_return_home=0;
            }


        }

        if(read_DS3231_timeout >= 30000) //30s
        {
            read_DS3231=true;
            read_DS3231_timeout=0;

        }
//***************************************************** DANG THUC HIEN GIAO DICH *****************************/
        if (purchase.state_old == screen_DANGTHUCHIENGIAODICH)
        {
            dangthuchiengiaodich_count++;

//            swap_home_display = 0;
//            home_display_count = 0;
//            home_animation = NO_CHANGE;
//            read_DS3231=false;
//            read_DS3231_timeout=0;

            if (dangthuchiengiaodich_count >= 10000)
            {
                purchase.transaction_status = FAILED;
                dangthuchiengiaodich_count = 0;
                purchase.animation_change = true;
            }

            else
            {
                purchase.animation_change = false;
            }

        }

//***************************************************** SANPHAM GIA SODU *****************************/

        if (purchase.state_old == screen_SANPHAM_GIA_SODU)
        {
            swap_home_display = 0;
            home_display_count = 0;
            home_animation = NO_CHANGE;
        }
    }
}

