/**
 * Production selection first - Case1
 */

// Đang thiếu tín hiệu từ drop sensor , mở cửa lấy hàng và từ bộ đọc tiền
#include "main.h"
#include "purchase.h"
#include "DS3231.h"

char SODU[15];
char GIA[15];

user_press_t purchase;
slot_information_t slot_location[TRACE][SLOT];
gettime_t time_now;

extern volatile uint8_t home_animation;
extern int number_buffer[];
extern const unsigned char nhietdo_mua1[], nhietdo_mua2[];

extern volatile uint32_t timeout_return_home;
extern volatile uint16_t dangthuchiengiaodich_count;
extern volatile bool read_DS3231;

void char_parse_int(char *source, uint32_t *desti)
{
    uint8_t int_temp[strlen (source)];
    uint8_t int_temp_cnt = 0;
    *desti = 0;
    for (int i = 0; i < strlen (source); i++)
    {
        if (source[i] <= '9' && source[i] >= '0')
        {
            int_temp[i] = source[i] - '0';
            int_temp_cnt++;

        }

    }

    for (int i = 0; i < int_temp_cnt; i++)
    {
        *desti = (*desti) * 10 + int_temp[i];
    }

}

void Slot_init(void)
{
    for (int i = 0; i < TRACE; i++)
    {
        for (int j = 0; j < SLOT; j++)
        {
            slot_location[i][j].dropsensor_off = false;
            slot_location[i][j].lock_status = false;
            slot_location[i][j].cost = 10000 + i * 5 * 1000;

        }

    }

}

void Purchase(void)
{

    sprintf (SODU, "%dVND", purchase.internal_money);
    switch (purchase.state)
    {
        case screen_HOME_SCREEN:

            if (purchase.case_purchase == case_INSERT_MONEY_FIRST) // when the customer inserts the money first
            {
                purchase.state = screen_MOIBANCHONSANPHAM;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

            if ((purchase.press_value_old != 0) && (purchase.press_value_old != '#') // number 0 to 9
                    && (purchase.press_value_old != '*'))
            {

                purchase.state = screen_SANPHAM_GIA_SODU;
                purchase.state_old = purchase.state;
                break;

            }

            if (purchase.press_value_old == '*') // ENTER
            {
                purchase.state_old = purchase.state;
                purchase.state = screen_SCAN_KEYPAD;
                purchase.press_count = 0;
                purchase.press_value_old = 0;

                break;
            }

            else if (purchase.press_value_old == '#') //CANCEL
            {
                purchase.state = screen_TIEPTUCMUAHANG_NHANTIENTHUA;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

            if (purchase.animation_change == false && home_animation != CHANGE2)
            {
                Home_display ();
                ST7920_DrawBitmap_AnyLocation (nhietdo_mua2, 36, 6, 10, 0);

            }

            else
            {

                if (read_DS3231 == true)
                {
                    char day_temp[20];
                    read_DS3231 = false;
                    Get_Time (&time_now);
                    sprintf (day_temp, "%d/%d/%d", time_now.dayofmonth, time_now.month, time_now.year);
                    ST7920_GraphicMode (DISABLE);
                    ST7920_SendString (0, 0, day_temp);

                    ST7920_GraphicMode (ENABLE);
                    Draw_number (number_buffer[(time_now.hour)/10], 0, 12);
                    Draw_number (number_buffer[(time_now.hour)%10], 2, 12);
                    Draw_number (number_buffer[10], 4, 12);
                    Draw_number (number_buffer[(time_now.minutes)/10], 6, 12);
                    Draw_number (number_buffer[(time_now.minutes)%10], 8, 12);

                }

                if (home_animation == CHANGE1)
                {
                    //ST7920_GraphicMode (ENABLE);
                    ST7920_DrawBitmap_AnyLocation (nhietdo_mua1, 36, 6, 10, 0);

                }
                else if (home_animation == NO_CHANGE)
                {
                    //ST7920_GraphicMode (ENABLE);
                    ST7920_DrawBitmap_AnyLocation (nhietdo_mua2, 36, 6, 10, 0);

                }

                else if (home_animation == CHANGE3)
                {

                    Home_display ();
                    ST7920_DrawBitmap_AnyLocation (nhietdo_mua2, 36, 6, 10, 0);
                }

                else
                {
                    Moibanchonsanpham_tien ();
                }

                purchase.animation_change = false;

            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;

        break;

        case screen_SANPHAM_GIA_SODU:

            if (purchase.press_count == 1 && purchase.press_value_old != '*' && purchase.press_value_old != '#') // * - ENTER , # - CANCEL
            {
                Gia_sodu (); // 1 is First number
                purchase.slot_user_select = purchase.press_value_old - '0';
                Draw_number (number_buffer[purchase.press_value_old - '0'], 6, 17);

            }

            else if (purchase.press_count == 2 && purchase.press_value_old != '*' && purchase.press_value_old != '#')
            {

                // 2 is Last number
                purchase.slot_user_select = (purchase.slot_user_select) * 10 + (purchase.press_value_old - '0');
                Draw_number (number_buffer[purchase.press_value_old - '0'], 8, 17);
                purchase.press_count = 0;

            }

            if (purchase.slot_user_select > (SLOT * TRACE) || purchase.slot_user_select == 0)
            {
                ST7920_GraphicMode (DISABLE);
                ST7920_SendString (3, 3, SODU);
                ST7920_SendString (0, 3, "         ");
                ST7920_GraphicMode (ENABLE);

                if (purchase.press_value_old == '*')
                {
                    purchase.press_count = purchase.press_count - 1;
                    Okhongtontai ();
                    R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_SECONDS);

                    purchase.state = screen_HOME_SCREEN;
                    purchase.state_old = purchase.state;
                    purchase.press_count = 0;
                    purchase.press_value_old = 0;
                    break;
                }

            }

            else
            {

                purchase.cost_item = slot_location[(purchase.slot_user_select - 1) / SLOT][(purchase.slot_user_select
                        - 1) % SLOT].cost;
                sprintf (GIA, "%dVND", purchase.cost_item);

                ST7920_GraphicMode (DISABLE);
                ST7920_SendString (3, 3, SODU);
                ST7920_SendString (0, 3, GIA);
                ST7920_GraphicMode (ENABLE);

                if (purchase.press_value_old == '*') //ENTER
                {
                    purchase.state = screen_SOSANHSODU;
                    purchase.state_old = purchase.state;
                    purchase.press_count = 0;
                    purchase.press_value_old = 0;
                    purchase.transaction_status = NONE;

                    break;

                }

            }

            if (purchase.press_value_old == '#') //CANCEL
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;
            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;

        break;

        case screen_SOSANHSODU:

            if ((purchase.internal_money < purchase.cost_item) && purchase.press_count == 0)
            {
                Sodukhongdu_vuilongthemtien ();

            }
            else if (purchase.internal_money >= purchase.cost_item) //purchase.internal_money>=purchase.cost_item
            {
                purchase.state = screen_DANGTHUCHIENGIAODICH;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;

                purchase.transaction_status = NONE;
                break;

            }

            if ((purchase.press_value_old != 0) && (purchase.press_value_old != '#') // number 0 to 9
                    && (purchase.press_value_old != '*'))
            {

                purchase.state = screen_SANPHAM_GIA_SODU;
                purchase.state_old = purchase.state;
                break;

            }

            else if (purchase.press_value_old == '#') //CANCEL
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;
            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;

        break;

        case screen_DANGTHUCHIENGIAODICH:

            if (purchase.press_count == 0)
            {
                Dangthuchiengiaodich ();
            }

            if (purchase.transaction_status == FAILED) // Ty doi lai thanh FAileed , purchase.transaction_status doi tin hieu tu drop sensor
            {
                purchase.state = screen_VUILONGCHONSANPHAMKHAC;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                purchase.transaction_status = NONE;

                break;

            }

            else if (purchase.transaction_status == SUCCESS)
            {

                dangthuchiengiaodich_count = 0; // Reset timeout
                purchase.internal_money = purchase.internal_money - purchase.cost_item;

                memset (SODU, 0, strlen (SODU));

                sprintf (SODU, "%dVND", purchase.internal_money);

                purchase.state = screen_MOIBANNHANSANPHAM;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                purchase.transaction_status = NONE;
                break;
            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;
            purchase.press_value_old = 0;

        break;

        case screen_VUILONGCHONSANPHAMKHAC:

            if (purchase.press_count == 0 && purchase.is_internal_money_change == false)
            {
                Muahangthatbai ();
                R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_SECONDS);
                Vuilongchonsanphamkhac ();
            }

            if ((purchase.press_value_old != 0) && (purchase.press_value_old != '#')
                    && (purchase.press_value_old != '*'))
            {

                purchase.state = screen_SANPHAM_GIA_SODU;
                purchase.state_old = purchase.state;

                break;

            }

            if (purchase.press_value_old == '#') //CANCEL
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;
            }

            if (purchase.press_value_old == '*') //ENTER
            {
                ;
            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;

        break;

        case screen_MOIBANNHANSANPHAM:

            if (purchase.press_count == 0)
            {
                Muahangthanhcong ();
                R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_SECONDS);
                Moibannhansanpham ();
                R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_SECONDS);
            }

            if (purchase.internal_money == 0) // Internal money = 0
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

            else
            {
                purchase.state = screen_TIEPTUCMUAHANG_NHANTIENTHUA;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;
            purchase.press_value_old = 0;

        break;

        case screen_TIEPTUCMUAHANG_NHANTIENTHUA:
            if (purchase.press_count == 0)
            {
                Tieptucmuahang_nhanlaitienthua ();
            }

            if (purchase.press_value_old == '1') // Keep buying
            {
                purchase.state = screen_MOIBANCHONSANPHAM;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;
            }

            else if (purchase.press_value_old == '2') //Return money to customer
            {

                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                purchase.internal_money = 0; // Return money
                break;
                // tra tien thua
            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;
            purchase.press_value_old = 0;

        break;

        case screen_MOIBANCHONSANPHAM:

            purchase.case_purchase = case_SELECT_ITEMS_FIRST; //back to default cases

            if (purchase.press_value_old == '*')
            {
                purchase.state_old = purchase.state;
                purchase.state = screen_SCAN_KEYPAD;
                purchase.press_count = 0;
                purchase.press_value_old = 0;

                break;
            }

            else if (purchase.press_value_old == '#')
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

            Moibanchonsanpham_tien ();

            if ((purchase.press_value_old != 0) && (purchase.press_value_old != '#') // number 0 to 9
                    && (purchase.press_value_old != '*'))
            {

                purchase.state = screen_SANPHAM_GIA_SODU;
                purchase.state_old = purchase.state;
                break;

            }

            purchase.state_old = purchase.state;
            purchase.state = screen_SCAN_KEYPAD;
            purchase.press_count = 0;

        break;

        case screen_SCAN_KEYPAD:

            purchase.is_internal_money_change = false;

            purchase.press_value = get_char_press ();
            if (purchase.press_value)
            {
                purchase.press_count++;

                purchase.state = purchase.state_old;
                purchase.press_value_old = purchase.press_value;
            }

            if (purchase.animation_change == true)
            {
                purchase.state = purchase.state_old;
                //purchase.animation_change=false;
            }

            if (timeout_return_home >= TIME_RETURN_HOME) // Return to Home Display
            {
                purchase.state = screen_HOME_SCREEN;
                purchase.state_old = purchase.state;
                purchase.press_count = 0;
                purchase.press_value_old = 0;
                break;

            }

#ifdef ENABLE_TEST_VINH
            Testt_sensor ();

#else

#endif

        break;

        default:
            purchase.animation_change = false;
            purchase.case_purchase = case_SELECT_ITEMS_FIRST; // default case
            purchase.cost_item = 0;
            purchase.internal_money = 0;
            purchase.press_count = 0;
            purchase.press_value = 0;
            purchase.press_value_old = 0;
            purchase.slot_user_select = 0;
            purchase.state = screen_HOME_SCREEN; // default display
            purchase.transaction_status = NONE;
            purchase.is_internal_money_change = false;

            //***********************For test************************************//
            Slot_init ();
            // purchase.case_purchase = case_INSERT_MONEY_FIRST;
            //*******************************************************************//

        break;

    }

}

