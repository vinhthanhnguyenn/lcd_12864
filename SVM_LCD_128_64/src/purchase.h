#ifndef PURCHASE_H_
#define PURCHASE_H_

#include "main.h"
#define NONE 0
#define SUCCESS 1
#define FAILED 2

#define TIME_RETURN_HOME 30000

typedef enum
{
    case_SELECT_ITEMS_FIRST = 0,
    case_INSERT_MONEY_FIRST,
} case_purchase_t;

typedef enum
{
    screen_HOME_SCREEN = 1,
    screen_SANPHAM_GIA_SODU,
    screen_SOSANHSODU,
    screen_DANGTHUCHIENGIAODICH,
    screen_VUILONGCHONSANPHAMKHAC,
    screen_MOIBANNHANSANPHAM,
    screen_TIEPTUCMUAHANG_NHANTIENTHUA,
    screen_MOIBANCHONSANPHAM,
    screen_SCAN_KEYPAD,

} state_machine;

typedef struct
{

    volatile state_machine state;
    volatile state_machine state_old;
    uint8_t press_count;
    uint8_t press_value;
    uint8_t press_value_old;
    volatile bool animation_change;
    uint8_t transaction_status;
    uint32_t internal_money;
    uint32_t cost_item;
    uint8_t slot_user_select;
    case_purchase_t case_purchase;
    bool is_internal_money_change;

} user_press_t;

void Purchase(void);

#endif
