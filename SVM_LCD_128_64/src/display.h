#ifndef DISPLAY_H
#define DISPLAY_H

#include "main.h"

#define LINK_QR "https://payment.momo.vn/pay/store/MOMOIHNE20210605-hyperlogy01?a=5000&b=1P5BGE&s=486efdefff1ceda083009de86cb4ac17fdc173691e6fa8a207d4ed1e385236d5"


enum animation_change
{
    NO_CHANGE=0,
    CHANGE1,
    CHANGE2,
    CHANGE3,

};

void Home_display(void);
void Draw_number(const unsigned char *image, uint8_t start_x_in_byte, uint8_t start_y);
void Moibanchonsanpham(void);
void Dangthuchiengiaodich(void);
void Moibannhansanpham(void);
void Moibanchonsanpham_tien(void);
void Gia_sodu(void);
void QR_thongtinsanpham(void);
void Vuilongchonsanphamkhac(void);
void Phuongthucthanhtoan(void);
void Tieptucmuahang_nhanlaitienthua(void);
void Sodukhongdu_vuilongthemtien(void);
void Muahangthanhcong(void);
void Muahangthatbai(void);


#endif
