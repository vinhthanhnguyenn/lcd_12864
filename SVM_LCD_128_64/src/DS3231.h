#ifndef DS3231_H
#define DS3231_H
#include "main.h"

#define I2C_TRANSACTION_BUSY_DELAY 1000;
#define DS3231_ADDRESS 0x68
#define EEPROM 0x50

#define PAGE_SIZE  64
#define PAGE_NUMBER 512
#define WORD_NUMBER_BYTE 6 // because 7 bits for WORD_NUMBER_PAGE ,5 bits for WORD_NUMBER_BYTE

typedef struct
{

    uint8_t seconds;
    uint8_t minutes;
    uint8_t hour;
    uint8_t dayofweek;
    uint8_t dayofmonth;
    uint8_t month;
    uint8_t year;

} gettime_t;

bool I2C_Mem_Write(uint8_t slave_address, uint16_t mem_address, uint8_t *data, uint16_t length_of_data);
bool I2C_Mem_Read(uint8_t slave_address, uint16_t mem_address, uint8_t *receiver_buffer, uint16_t bytes_of_buffer);

//*************************USER FUNCTION***************************************/

bool Set_Time(uint8_t sec, uint8_t min, uint8_t hour, uint8_t day, uint8_t date, uint8_t month, uint8_t year);
bool Get_Time(gettime_t *time);
void EEPROM_Write(uint16_t Page, uint16_t Offset, uint8_t *data, uint16_t Size);
void EEPROM_Read(uint16_t Page, uint16_t Offset, uint8_t *data, uint16_t Size);
void EEPROM_Errase(uint16_t Page, uint16_t Offset, uint16_t Size);
void I2C_Init(void);


#endif
