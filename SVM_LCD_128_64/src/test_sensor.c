#include "test_sensor.h"

volatile uint8_t test_momo_qr = NONE; // test, 1 là quét mã thành công , 0 là thất bại
volatile uint8_t test_credit_card = NONE; // test, 1 là thanh toán bằng thẻ thành công , 0 là thất bại

extern user_press_t purchase;

void Testt_sensor(void)
{
    bsp_io_level_t naptien = BSP_IO_LEVEL_HIGH;
    bsp_io_level_t dropsensor = BSP_IO_LEVEL_HIGH;

    R_IOPORT_PinRead (&g_ioport_ctrl, THEMTIEN10K, &naptien);
    R_IOPORT_PinRead (&g_ioport_ctrl, DROPSENSOR, &dropsensor);

    R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, BSP_IO_LEVEL_LOW);

    while (naptien == BSP_IO_LEVEL_LOW)
    {
        R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, BSP_IO_LEVEL_HIGH);
        // R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
        R_IOPORT_PinRead (&g_ioport_ctrl, THEMTIEN10K, &naptien);
        while (naptien == BSP_IO_LEVEL_LOW)
        {
            R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
            R_IOPORT_PinRead (&g_ioport_ctrl, THEMTIEN10K, &naptien);
        }
        purchase.is_internal_money_change=true;
        purchase.internal_money = purchase.internal_money + 10000;
        purchase.state = purchase.state_old;

    }

    while (dropsensor == BSP_IO_LEVEL_LOW)
    {
        R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, BSP_IO_LEVEL_HIGH);
        R_IOPORT_PinRead (&g_ioport_ctrl, DROPSENSOR, &dropsensor);

        while (dropsensor == BSP_IO_LEVEL_LOW)
        {
            R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
            R_IOPORT_PinRead (&g_ioport_ctrl, DROPSENSOR, &dropsensor);
        }
        if (purchase.state_old == screen_DANGTHUCHIENGIAODICH)
        {
            purchase.transaction_status = SUCCESS; // Có hàng qua drop sensor
            purchase.state = purchase.state_old;
        }

    }

}

