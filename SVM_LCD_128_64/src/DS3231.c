#include "DS3231.h"
#include "hal_data.h"




volatile bool i2c_tx_flag = false;
volatile bool i2c_rx_flag = false;

volatile bool i2c_sci_tx_flag = false;
volatile bool i2c_sci_rx_flag = false;

/**
 * Checking transmission complete or not
 * @return true or false
 */

bool Check_TX_flag(void)
{
    uint32_t timeout_ms = I2C_TRANSACTION_BUSY_DELAY
    ;
    while ((i2c_tx_flag == false) && timeout_ms)
    {
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);
        timeout_ms--;

    }
    i2c_tx_flag = false;
    if (timeout_ms == 0)
    {

        return false;

    }

    return true;
}

/**
 * Checking receive complete or not
 * @return true or false
 */

bool Check_RX_flag(void)
{
    uint32_t timeout_ms = I2C_TRANSACTION_BUSY_DELAY
    ;
    while ((i2c_rx_flag == false) && timeout_ms)
    {
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);
        timeout_ms--;
    }

    i2c_rx_flag = false;
    if (timeout_ms == 0)
    {
        return false;

    }

    return true;
}

bool Check_TX_SCI_flag(void)
{
    uint32_t timeout_ms = I2C_TRANSACTION_BUSY_DELAY
    ;
    while ((i2c_sci_tx_flag == false) && timeout_ms)
    {
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);
        timeout_ms--;

    }
    i2c_sci_tx_flag = false;
    if (timeout_ms == 0)
    {

        return false;

    }

    return true;
}

/**
 * Checking receive complete or not
 * @return true or false
 */

bool Check_RX_SCI_flag(void)
{
    uint32_t timeout_ms = I2C_TRANSACTION_BUSY_DELAY
    ;
    while ((i2c_sci_rx_flag == false) && timeout_ms)
    {
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);
        timeout_ms--;
    }

    i2c_sci_rx_flag = false;
    if (timeout_ms == 0)
    {
        return false;

    }

    return true;
}

// Convert normal decimal numbers to binary coded decimal
uint8_t DecToBCD(int val)
{
    return (uint8_t) ((val / 10 * 16) + (val % 10));
}

// Convert binary coded decimal to normal decimal numbers
int BCDToDec(uint8_t val)
{
    return (int) ((val / 16 * 10) + (val % 16));
}

/**
 * Write data to any address of the device
 * @param mem_address : Starting address that the users want to write
 * @param data: Stream of data
 * @param length_of_data : length of bytes
 * @return
 */

bool I2C_Mem_Write(uint8_t slave_address, uint16_t mem_address, uint8_t *data, uint16_t length_of_data)
{
    if (slave_address == DS3231_ADDRESS)
    {
        uint8_t send_buffer[length_of_data + 1];
        for (int i = 0; i < (length_of_data + 1); i++)
        {
            if (i == 0)
            {
                send_buffer[0] = (uint8_t) mem_address;
            }
            else
            {
                send_buffer[i] = data[i - 1];

            }

        }
        R_IIC_MASTER_Write (&g_i2c_master0_ctrl, send_buffer, (length_of_data + 1), false);

        if (Check_TX_flag () == false)
        {
            return false;
        }

    }

    else if (slave_address == EEPROM) // EEPROM
    {
        uint8_t send_buffer[length_of_data + 2];
        for (int i = 0; i < (length_of_data + 2); i++)
        {
            if (i == 0)
            {
                send_buffer[0] = (uint8_t) (mem_address >> 8);
            }

            else if (i == 1)
            {
                send_buffer[1] = (uint8_t) (mem_address);
            }
            else
            {
                send_buffer[i] = data[i - 2];

            }
        }

        R_SCI_I2C_Write (&g_i2c1_ctrl, send_buffer, (length_of_data + 2), false);

        if (Check_TX_SCI_flag () == false)
        {
            return false;
        }

    }

    return true;

}

/**
 * Read data from any starting address of the device
 * @param mem_address : Starting address that the users want to read
 * @param receiver_buffer :  buffer to save
 * @param bytes_of_buffer : Length of bytes
 * @return
 */

bool I2C_Mem_Read(uint8_t slave_address, uint16_t mem_address, uint8_t *receiver_buffer, uint16_t bytes_of_buffer)
{
    if (slave_address == DS3231_ADDRESS)
    {
        R_IIC_MASTER_Write (&g_i2c_master0_ctrl, &mem_address, 1, true);
        if (Check_TX_flag () == false)
        {
            return false;
        }
        R_IIC_MASTER_Read (&g_i2c_master0_ctrl, receiver_buffer, bytes_of_buffer, false);

        if (Check_RX_flag () == false)
        {
            return false;
        }

    }

    else if (slave_address == EEPROM) // EEPROM
    {
        uint8_t eeprom_mem_add_temp[2];
        eeprom_mem_add_temp[0] = (uint8_t) (mem_address >> 8);
        eeprom_mem_add_temp[1] = (uint8_t) (mem_address);

        R_SCI_I2C_Write (&g_i2c1_ctrl, eeprom_mem_add_temp, 2, true);

        if (Check_TX_SCI_flag () == false)
        {
            return false;
        }
        R_SCI_I2C_Read (&g_i2c1_ctrl, receiver_buffer, bytes_of_buffer, false);

        if (Check_RX_SCI_flag () == false)
        {
            return false;
        }

    }

    return true;

}

/* function to set time */

bool Set_Time(uint8_t sec, uint8_t min, uint8_t hour, uint8_t day, uint8_t date, uint8_t month, uint8_t year)
{
    uint8_t set_time[7];

    set_time[0] = DecToBCD (sec);
    set_time[1] = DecToBCD (min);
    set_time[2] = DecToBCD (hour);
    set_time[3] = DecToBCD (day);
    set_time[4] = DecToBCD (date);
    set_time[5] = DecToBCD (month);
    set_time[6] = DecToBCD (year);
    if (I2C_Mem_Write (DS3231_ADDRESS, 0x00, &set_time[0], 7) == false)
    {
        return false;
    }

    return true;

}

/**
 * Function to get time
 * @param time : A variable type gettime_t
 * @return : true or false
 */

bool Get_Time(gettime_t *time)
{
    uint8_t get_time[7];
    if (I2C_Mem_Read (DS3231_ADDRESS, 0x00, get_time, 7) == false)
    {
        return false;
    }
    time->seconds = BCDToDec (get_time[0]);
    time->minutes = BCDToDec (get_time[1]);
    time->hour = BCDToDec (get_time[2]);
    time->dayofweek = BCDToDec (get_time[3]);
    time->dayofmonth = BCDToDec (get_time[4]);
    time->month = BCDToDec (get_time[5]);
    time->year = BCDToDec (get_time[6]);
    return true;
}

uint16_t Data_Seperate(uint16_t offset, uint16_t size)
{
    if ((offset + size) > PAGE_SIZE)
    {
        return (PAGE_SIZE - offset);

    }

    else
    {
        return size;

    }
}

void EEPROM_Write(uint16_t Page, uint16_t Offset, uint8_t *data, uint16_t Size)
{
    // calculate the start page and the end page
    uint16_t startPage = Page;
    uint16_t endPage = Page + ((Offset + Size) / PAGE_SIZE); //Exam : Size = 50, Offset = 20 => 2 Page needed

    // number of pages to be written
    uint16_t numofpages = (endPage - startPage) + 1;
    uint16_t pos = 0;

    // write the data
    for (int i = 0; i < numofpages; i++)
    {
        uint16_t MemAddress = (startPage << WORD_NUMBER_BYTE) | Offset; //Exam: We are using page 3rd, write to byte 30th => (3<<6)|30
        uint16_t bytesremaining = Data_Seperate (Offset, Size);  // calculate the remaining bytes to be written
        I2C_Mem_Write (EEPROM, MemAddress, &data[pos], bytesremaining);
        startPage += 1;  // increment the page, so that a new page address can be selected for further write
        Offset = 0;   // since we will be writing to a new page, so offset will be 0
        Size = Size - bytesremaining; // The rest of size bytes
        pos += bytesremaining; // Update possision
        R_BSP_SoftwareDelay (20, BSP_DELAY_UNITS_MILLISECONDS);  // Write cycle delay (20ms)
    }

}

void EEPROM_Read(uint16_t Page, uint16_t Offset, uint8_t *data, uint16_t Size)
{
    // calculate the start page and the end page
    uint16_t startPage = Page;
    uint16_t endPage = Page + ((Offset + Size) / PAGE_SIZE); //Exam : Size = 50, Offset = 20 => 2 Page needed

    // number of pages to be written
    uint16_t numofpages = (endPage - startPage) + 1;
    uint16_t pos = 0;

    // write the data
    for (int i = 0; i < numofpages; i++)
    {
        uint16_t MemAddress = (startPage << WORD_NUMBER_BYTE) | Offset; //Exam: We are using page 3rd, write to byte 30th => (3<<6)|30
        uint16_t bytesremaining = Data_Seperate (Offset, Size);  // calculate the remaining bytes to be written
        I2C_Mem_Read (EEPROM, MemAddress, &data[pos], bytesremaining);
        startPage += 1;  // increment the page, so that a new page address can be selected for further write
        Offset = 0;   // since we will be writing to a new page, so offset will be 0
        Size = Size - bytesremaining; // The rest of size bytes
        pos += bytesremaining; // Update possision
        R_BSP_SoftwareDelay (20, BSP_DELAY_UNITS_MILLISECONDS); // Write cycle delay (20ms)
    }

}

void EEPROM_Errase(uint16_t Page, uint16_t Offset, uint16_t Size)
{
    uint8_t Errase[Size];
    memset (Errase, 0xff, Size);

    // calculate the start page and the end page
    uint16_t startPage = Page;
    uint16_t endPage = Page + ((Offset + Size) / PAGE_SIZE); //Exam : Size = 50, Offset = 20 => 2 Page needed

    // number of pages to be written
    uint16_t numofpages = (endPage - startPage) + 1;
    uint16_t pos = 0;

    // write the data
    for (int i = 0; i < numofpages; i++)
    {
        uint16_t MemAddress = (startPage << WORD_NUMBER_BYTE) | Offset; //Exam: We are using page 3rd, write to byte 30th => (3<<6)|30
        uint16_t bytesremaining = Data_Seperate (Offset, Size);  // calculate the remaining bytes to be written

        I2C_Mem_Write (EEPROM, MemAddress, &Errase[pos], bytesremaining);
        startPage += 1;  // increment the page, so that a new page address can be selected for further write
        Offset = 0;   // since we will be writing to a new page, so offset will be 0
        Size = Size - bytesremaining; // The rest of size bytes
        pos += bytesremaining; // Update possision
        R_BSP_SoftwareDelay (20, BSP_DELAY_UNITS_MILLISECONDS); //delay (5ms)
    }

}

/**
 * Function to initialize
 */

void I2C_Init(void)
{
    fsp_err_t err;
    err = R_IIC_MASTER_Open (&g_i2c_master0_ctrl, &g_i2c_master0_cfg);
    assert(FSP_SUCCESS == err);
    //
    err = R_SCI_I2C_Open (&g_i2c1_ctrl, &g_i2c1_cfg);
    assert(FSP_SUCCESS == err);

}

/**
 * Interrupt callback functions
 * @param p_args
 */

void I2C_Callback(i2c_master_callback_args_t *p_args)
{
    if (p_args->event == I2C_MASTER_EVENT_RX_COMPLETE)
    {
        i2c_rx_flag = true;
    }

    if (p_args->event == I2C_MASTER_EVENT_TX_COMPLETE)
    {
        i2c_tx_flag = true;
    }
}

void SCI_I2C_Callback(i2c_master_callback_args_t *p_args)
{
    if (p_args->event == I2C_MASTER_EVENT_RX_COMPLETE)
    {
        i2c_sci_rx_flag = true;
    }

    if (p_args->event == I2C_MASTER_EVENT_TX_COMPLETE)
    {
        i2c_sci_tx_flag = true;
    }
}

