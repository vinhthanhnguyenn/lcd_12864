#ifndef MAIN_H
#define MAIN_H


#include "hal_data.h"
#include "ST7920_SPI.h"
#include "stdio.h"
#include "string.h"
#include "qrcodegen.h"
#include <stdbool.h>
#include "display.h"
#include "keypad.h"
#include "purchase.h"

#include "test_sensor.h"
#include "DS3231.h"

#define SLOT 10
#define TRACE 6

#define ENABLE_TEST_VINH

typedef struct
{
    uint32_t cost;
    bool dropsensor_off;
    uint8_t capacity;
    bool lock_status;

}slot_information_t;


void User_failed(void);

#endif
