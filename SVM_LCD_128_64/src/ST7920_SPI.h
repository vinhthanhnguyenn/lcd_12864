#ifndef ST7920_H_
#define ST7920_H_
/*
 * Created by Vinh Thanh Nguyen 17092022 - Hyperlogy Copr
 */

#include "hal_data.h"

#define CS_PIN      BSP_IO_PORT_01_PIN_01
#define RST_PIN     BSP_IO_PORT_01_PIN_03


//CLK: 111
//Mosi:109

#define ENABLE  1
#define DISABLE 0

enum STATE_NUMBER
{
    NONE=0,
    ODD,
    EVEN,
};







void SendByteSPI(uint8_t byte);
void ST7920_SendCmd (uint8_t cmd);


void SetPixel(uint8_t x, uint8_t y);


/*********************************USER FUNCTION****************************************/
void SPI_Init(void);
void ST7920_Init (void);
void ST7920_SendData (uint8_t data);
void ST7920_SendString(uint8_t row, uint8_t col, char* string);
void ST7920_GraphicMode (int mode);
void ST7920_DrawBitmap(const unsigned char* graphic);
void ST7920_Clear(void);
bool ST7920_DrawBitmap_AnyLocation(const unsigned char *graphic, uint16_t number_row_in_buffer,uint16_t number_col_in_buffer, uint8_t start_x_in_byte, uint8_t start_y);
bool Draw_QR_Code (const char *link_text, uint8_t col_start,uint8_t row_start);




#endif
