/*
 * keypad.h
 *
 *  Created on: 10 Sep 2022
 *      Author: Trung
 */

#ifndef KEYPAD_H_
#define KEYPAD_H_

#include "hal_data.h"
#include "main.h"

#define ROWS    4
#define COLS    3

#define COL1    BSP_IO_PORT_01_PIN_02
#define COL2    BSP_IO_PORT_01_PIN_00
#define COL3    BSP_IO_PORT_03_PIN_02
#define ROW1    BSP_IO_PORT_04_PIN_02
#define ROW2    BSP_IO_PORT_01_PIN_13
#define ROW3    BSP_IO_PORT_03_PIN_03
#define ROW4    BSP_IO_PORT_01_PIN_05

void gpio_init(void);
uint8_t get_char_press(void);
void keypad_init(void);

#endif /* KEYPAD_H_ */
