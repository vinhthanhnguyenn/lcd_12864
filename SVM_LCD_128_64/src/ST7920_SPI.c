/**
 * ST7920 library for LCD 12864 communication
 *Created by Vinh Thanh Nguyen 17/09/2022 - Hyperlogy Corp
 */

#include "ST7920_SPI.h"
#include "hal_data.h"
#include "stdbool.h"
#include "qrcodegen.h"
uint8_t Graphic_Check = DISABLE;
static volatile bool g_transfer_complete = false;

uint8_t startRow, startCol, endRow, endCol; // coordinates of the dirty rectangle
uint8_t numRows = 64;
uint8_t numCols = 128;

uint8_t image[(128 * 64) / 8];

uint8_t image_buffer_replace[1024];
extern uint8_t image_buffer[1024];

/**
 * Write a byte to LCD 12864 in SPI protocol
 * @param byte: The bytes you want to write
 */

void SendByteSPI(uint8_t byte)
{
    /* Start a write/read transfer */
    g_transfer_complete = false;
    R_SPI_Write (&g_spi0_ctrl, &byte, 1, SPI_BIT_WIDTH_8_BITS);
    /* Wait for SPI_EVENT_TRANSFER_COMPLETE callback event. */
    while (false == g_transfer_complete)
    {
        ;
    }

}

/**
 * Send a command to LCD 12864
 * @param cmd: Command you want to write
 */
void ST7920_SendCmd(uint8_t cmd)
{

    R_IOPORT_PinWrite (&g_ioport_ctrl, CS_PIN, BSP_IO_LEVEL_HIGH);  // PUll the CS high

    SendByteSPI (0xf8 + (0 << 1));  // send the SYNC + RS(0)
    SendByteSPI (cmd & 0xf0);  // send the higher nibble first
    SendByteSPI ((uint8_t) ((cmd << 4) & 0xf0));  // send the lower nibble

    R_IOPORT_PinWrite (&g_ioport_ctrl, CS_PIN, BSP_IO_LEVEL_LOW);  // PUll the CS LOW

}

/**
 * Send a data byte (character) you want to display to LCD 12864
 * @param data
 */
void ST7920_SendData(uint8_t data)
{

    R_IOPORT_PinWrite (&g_ioport_ctrl, CS_PIN, BSP_IO_LEVEL_HIGH);  // PUll the CS high

    SendByteSPI (0xf8 + (1 << 1));  // send the SYNC + RS(1)
    SendByteSPI (data & 0xf0);  // send the higher nibble first
    SendByteSPI ((uint8_t) ((data << 4) & 0xf0));  // send the lower nibble
    R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MICROSECONDS);

    R_IOPORT_PinWrite (&g_ioport_ctrl, CS_PIN, BSP_IO_LEVEL_LOW); // PUll the CS LOW
}

/**
 * Send a string you want to display to LCD 12864
 * @param row : row
 * @param col : colum
 * @param string : Your string that you want to display
 */
void ST7920_SendString(uint8_t row, uint8_t col, char *string)
{
    switch (row)
    {
        case 0:
            col |= 0x80;
        break;
        case 1:
            col |= 0x90;
        break;
        case 2:
            col |= 0x88;
        break;
        case 3:
            col |= 0x98;
        break;
        default:
            col |= 0x80;
        break;
    }

    ST7920_SendCmd (col);

    while (*string)
    {
        ST7920_SendData ((uint8_t) (*string++));
    }
}

/**
 * Init LCD
 */
void ST7920_Init(void)
{
    SPI_Init();

    R_IOPORT_PinWrite (&g_ioport_ctrl, RST_PIN, BSP_IO_LEVEL_LOW);  // RESET=0
    R_BSP_SoftwareDelay (10, BSP_DELAY_UNITS_MILLISECONDS);   // wait for 10ms

    R_IOPORT_PinWrite (&g_ioport_ctrl, RST_PIN, BSP_IO_LEVEL_HIGH); // RESET=1
    R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);  //wait for >40 ms

    ST7920_SendCmd (0x30);  // 8bit mode
    R_BSP_SoftwareDelay (110, BSP_DELAY_UNITS_MICROSECONDS);  // 1ms delay  //  >100us delay

    ST7920_SendCmd (0x30);  // 8bit mode
    R_BSP_SoftwareDelay (40, BSP_DELAY_UNITS_MICROSECONDS);  // >37us delay

    ST7920_SendCmd (0x08);  // D=0, C=0, B=0
    R_BSP_SoftwareDelay (110, BSP_DELAY_UNITS_MICROSECONDS); // >100us delay

    ST7920_SendCmd (0x01);  // clear screen
    R_BSP_SoftwareDelay (12, BSP_DELAY_UNITS_MILLISECONDS);  // >10 ms delay

    ST7920_SendCmd (0x06);  // cursor increment right no shift
    R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay

    ST7920_SendCmd (0x0C);  // D=1, C=0, B=0
    R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS); // 1ms delay

    ST7920_SendCmd (0x02);  // return to home
    R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay

}

// switch to graphic mode or normal mode::: enable = 1 -> graphic mode enable = 0 -> normal mode

/**
 * Select Graphic mode or normal mode
 * @param mode : ENABLE or DISABLE
 */
void ST7920_GraphicMode(int mode)   // 1-enable, 0-disable
{
    if (mode == ENABLE)
    {
        ST7920_SendCmd (0x30);  // 8 bit mode
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay
        ST7920_SendCmd (0x34);  // switch to Extended instructions
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay
        ST7920_SendCmd (0x36);  // enable graphics
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay
        Graphic_Check = ENABLE;  // update the variable
    }

    else if (mode == DISABLE)
    {
        ST7920_SendCmd (0x30);  // 8 bit mode
        R_BSP_SoftwareDelay (1, BSP_DELAY_UNITS_MILLISECONDS);  // 1ms delay
        Graphic_Check = DISABLE;  // update the variable
    }
}

/**
 * Draw the image you want
 * @param graphic : Buffer contains the hex array of image
 */
void ST7920_DrawBitmap(const unsigned char *graphic)
{
    uint8_t x, y;
    for (y = 0; y < 64; y++)
    {
        if (y < 32)
        {
            for (x = 0; x < 8; x++)                          // Draws top half of the screen.
            { // In extended instruction mode, vertical and horizontal coordinates must be specified before sending data in.
                ST7920_SendCmd (0x80 | y);               // Vertical coordinate of the screen is specified first. (0-31)
                ST7920_SendCmd (0x80 | x);               // Then horizontal coordinate of the screen is specified. (0-8)
                ST7920_SendData (graphic[2 * x + 16 * y]);       // Data to the upper byte is sent to the coordinate.
                ST7920_SendData (graphic[2 * x + 1 + 16 * y]); // Data to the lower byte is sent to the coordinate.
            }
        }
        else
        {
            for (x = 0; x < 8; x++)                          // Draws bottom half of the screen.
            {                                               // Actions performed as same as the upper half screen.
                ST7920_SendCmd ((uint8_t) (0x80 | (y - 32))); // Vertical coordinate must be scaled back to 0-31 as it is dealing with another half of the screen.
                ST7920_SendCmd (0x88 | x);
                ST7920_SendData (graphic[2 * x + 16 * y]);
                ST7920_SendData (graphic[2 * x + 1 + 16 * y]);
            }
        }

    }
}

/**
 *
 * @param graphic: the buffer contain a image
 * @param number_row_in_buffer: the number of row that you want to draw , 0< number_row_in_buffer <=16 *note: number_row is number of pixel
 * @param number_col_in_buffer: the number of column that you want to draw, 0< number_col_in_buffer <=16
 * @param start_x_in_byte: the begin of x coordinate is 0 ,  x must be even number , 0< x <= 16
 * @param start_y: the begin of y coordinate is 0 ,  0 <= start_y < 64
 */

bool ST7920_DrawBitmap_AnyLocation(const unsigned char *graphic, uint16_t number_row_in_buffer ,
        uint16_t number_col_in_buffer, uint8_t start_x_in_byte, uint8_t start_y)
{
    uint8_t x, y;
    uint8_t state_number_of_col = NONE; // NONE,ODD and EVEN is state of column
    uint8_t number_col_in_address;
    uint8_t start_x_in_address;
    // uint8_t temp_replace[number_col_in_buffer+1]={0};
    start_x_in_address = start_x_in_byte / 2;

    if (number_col_in_buffer % 2 == 0)
    {
        number_col_in_address = (uint8_t)number_col_in_buffer / 2;
        state_number_of_col = EVEN;
    }

    else
    {
        number_col_in_address = (uint8_t)(number_col_in_buffer / 2) + 1;
        state_number_of_col = ODD;
        uint32_t count_replace = 0;
        for (uint32_t i = 0; i < ((number_row_in_buffer * number_col_in_buffer)); i++)
        {
            if ((i % number_col_in_buffer == 0) && (i != 0))
            {
                image_buffer_replace[count_replace] = 0x00;
                count_replace++;
            }

            image_buffer_replace[count_replace] = graphic[i];
            count_replace++;

        }

        number_col_in_buffer = number_col_in_buffer + 1;

    }

    if (((number_row_in_buffer + start_y) > 64) || ((number_col_in_buffer + start_x_in_byte) > 16)||(start_x_in_byte %2 !=0))
    {
        User_failed();
        return false;
    }

    for (y = start_y; y < (number_row_in_buffer + start_y); y++) //1 4
    {
        if (y < 32)
        {
            for (x = start_x_in_address; x < (start_x_in_address + number_col_in_address); x++) // Draws top half of the screen. //1 1+3
            { // In extended instruction mode, vertical and horizontal coordinates must be specified before sending data in.
                ST7920_SendCmd (0x80 | y);           // Vertical coordinate of the screen is specified first. (0-31)
                ST7920_SendCmd (0x80 | x);           // Then horizontal coordinate of the screen is specified. (0-8)
                if (state_number_of_col == EVEN)
                {
                    ST7920_SendData (
                            graphic[2 * (x - start_x_in_address) + (number_col_in_address * 2) * (y - start_y)]); // Data to the upper byte is sent to the coordinate.
                    ST7920_SendData (
                            graphic[(2 * (x - start_x_in_address) + 1) + (number_col_in_address * 2) * (y - start_y)]); // Data to the lower byte is sent to the coordinate.
                }

                else if (state_number_of_col == ODD)
                {
                    ST7920_SendData (
                            image_buffer_replace[2 * (x - start_x_in_address)
                                    + (number_col_in_address * 2) * (y - start_y)]); // Data to the upper byte is sent to the coordinate.
                    ST7920_SendData (
                            image_buffer_replace[(2 * (x - start_x_in_address) + 1)
                                    + (number_col_in_address * 2) * (y - start_y)]); // Data to the lower byte is sent to the coordinate.

                }

            }
        }
        else
        {
            for (x = start_x_in_address; x < (start_x_in_address + number_col_in_address); x++) // Draws bottom half of the screen.
            {                                               // Actions performed as same as the upper half screen.
                ST7920_SendCmd ((uint8_t)(0x80 | (y - 32)));    // Vertical coordinate of the screen is specified first. (0-31)
                ST7920_SendCmd (0x88 | x);           // Then horizontal coordinate of the screen is specified. (0-8)
                if (state_number_of_col == EVEN)
                {
                    ST7920_SendData (
                            graphic[2 * (x - start_x_in_address) + (number_col_in_address * 2) * (y - start_y)]); // Data to the upper byte is sent to the coordinate.
                    ST7920_SendData (
                            graphic[(2 * (x - start_x_in_address) + 1) + (number_col_in_address * 2) * (y - start_y)]); // Data to the lower byte is sent to the coordinate.
                }

                else if (state_number_of_col == ODD)
                {
                    ST7920_SendData (
                            image_buffer_replace[2 * (x - start_x_in_address)
                                    + (number_col_in_address * 2) * (y - start_y)]); // Data to the upper byte is sent to the coordinate.
                    ST7920_SendData (
                            image_buffer_replace[(2 * (x - start_x_in_address) + 1)
                                    + (number_col_in_address * 2) * (y - start_y)]); // Data to the lower byte is sent to the coordinate.

                }
            }
        }

    }

    return true;

}


/**
 * Draw the user link QR code
 * @param link_text : user link QR text
 * @param col_start : start of column user want to draw ( *note:col_start must be even number)
 * @param row_start : start of row user want to draw
 * @return : true if success , otherwise
 */

bool Draw_QR_Code (const char *link_text, uint8_t col_start,uint8_t row_start)
{
    uint16_t col_qr_code, row_qr_code,size_qr_code;
    size_qr_code=Gen_QR(link_text);
        if(size_qr_code==0)
        {
            User_failed();
        }
        row_qr_code=size_qr_code;
        col_qr_code= (uint16_t)(((size_qr_code/8)+1));

        if(ST7920_DrawBitmap_AnyLocation(image_buffer, row_qr_code, col_qr_code, col_start, row_start)==true)
        {
            return true;
        }

        return false;

}

// set Pixel

void SetPixel(uint8_t x, uint8_t y)
{
    if (y < numRows && x < numCols)
    {
        uint8_t *p = image + ((y * (numCols / 8)) + (x / 8));
        *p |= 0x80u >> (x % 8);

        *image = *p;

        // Change the dirty rectangle to account for a pixel being dirty (we assume it was changed)
        if (startRow > y)
        {
            startRow = y;
        }
        if (endRow <= y)
        {
            endRow = y + 1;
        }
        if (startCol > x)
        {
            startCol = x;
        }
        if (endCol <= x)
        {
            endCol = x + 1;
        }

    }

}

/**
 * Clear all display
 */
void ST7920_Clear(void)
{
    //***************************GRAPHIC MODE CLEAR******************************//
        ST7920_GraphicMode(ENABLE);
        uint8_t x, y;
        for (y = 0; y < 64; y++)
        {
            if (y < 32)
            {
                ST7920_SendCmd (0x80 | y);
                ST7920_SendCmd (0x80);
            }
            else
            {
                ST7920_SendCmd ((uint8_t) (0x80 | (y - 32)));
                ST7920_SendCmd (0x88);
            }
            for (x = 0; x < 8; x++)
            {
                ST7920_SendData (0);
                ST7920_SendData (0);
            }
        }
        //***************************NORMAL******************************//
        R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_MILLISECONDS); // delay >1.6 ms

        ST7920_GraphicMode(DISABLE);
        ST7920_SendCmd (0x01);   // clear the display using command
        R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_MILLISECONDS); // delay >1.6 ms

}

/**
 * Init for SPI peripheral
 */
void SPI_Init(void)
{
    fsp_err_t err = FSP_SUCCESS;
    /* Initialize the SPI module. */
    err = R_SPI_Open (&g_spi0_ctrl, &g_spi0_cfg);
    /* Handle any errors. This function should be defined by the user. */
    assert(FSP_SUCCESS == err);

}

void spi_callback(spi_callback_args_t *p_args)
{
    if (SPI_EVENT_TRANSFER_COMPLETE == p_args->event)
    {
        g_transfer_complete = true;
    }
}

