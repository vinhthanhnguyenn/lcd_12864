################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/DS3231.c \
../src/ST7920_SPI.c \
../src/display.c \
../src/hal_entry.c \
../src/keypad.c \
../src/purchase.c \
../src/qrcodegen.c \
../src/test_sensor.c 

C_DEPS += \
./src/DS3231.d \
./src/ST7920_SPI.d \
./src/display.d \
./src/hal_entry.d \
./src/keypad.d \
./src/purchase.d \
./src/qrcodegen.d \
./src/test_sensor.d 

OBJS += \
./src/DS3231.o \
./src/ST7920_SPI.o \
./src/display.o \
./src/hal_entry.o \
./src/keypad.o \
./src/purchase.o \
./src/qrcodegen.o \
./src/test_sensor.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Arm Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g -D_RA_CORE=CM33 -D_RENESAS_RA_ -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\src" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra\fsp\inc" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra\fsp\inc\api" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra\fsp\inc\instances" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra\arm\CMSIS_5\CMSIS\Core\Include" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra_gen" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra_cfg\fsp_cfg\bsp" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra_cfg\fsp_cfg" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra_cfg\freertos" -I"C:\Users\ADMIN\e2_studio\workspace\SVM_LCD_128_64\ra_cfg\fsp_cfg\middleware" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


